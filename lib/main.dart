import 'package:flutter/material.dart';

void main() => runApp(WeatherApp(
));

class WeatherApp extends StatefulWidget{
  @override
  _WeatherWigetState createState() => _WeatherWigetState();
}

class _WeatherWigetState extends State<WeatherApp>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
  AppBar buildAppBarWidget(){
    return AppBar(
      title: Text("พยากรณ์อากาศ"),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.cloud),
          color: Colors.lightBlueAccent,
          onPressed: (){
            print("Contact");
          },
        )

      ],
    );
  }

  Widget buildBodyWidget() {
    return ListView(
      children: <Widget>[
        Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 250,
                child: Image.network(
                  "https://images.unsplash.com/photo-1513002749550-c59d786b8e6c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80",
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                height: 60,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,

                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(15.0),
                      child :Text(
                        "มีเมฆเป็นบางส่วนระหว่างเวลา 03:00-04:00",
                        style: TextStyle(fontSize: 19),
                      ),
                    ),
                  ],
                ),
              ),

              Divider(
                color: Colors.lightBlueAccent,
              ),

              Container(
                margin: const EdgeInsets.only(top: 4, bottom: 4),
                child : Theme (
                  data: ThemeData (
                    iconTheme: IconThemeData (
                      color: Colors.lightBlueAccent,
                    ),
                  ),
                  child: profileActionItems(),
                ),
              ),

              Divider(
                color: Colors.lightBlueAccent,
              ),

              Text("พยากรณ์อากาศ 7 วัน"),

              DayNowListTile(),
              DayOneListTile(),
              DayTwoListTile(),
              DayTreeListTile(),
              DayFourListTile(),
              DayFiveListTile(),
              DaySixListTile(),

            ]
        )
      ],
    );
  }

  Widget profileActionItems() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildNowButton(),
        buildOneButton(),
        buildTwoButton(),
        buildThreeButton(),
        buildFourButton(),
        buildFiveButton(),
        buildSixButton(),
      ],
    );
  }

  Widget buildNowButton() {
    return Column(
      children: <Widget>[
        Text("ตอนนี้"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("23 องศา"),

      ],
    );
  }

  Widget buildOneButton() {
    return Column(
      children: <Widget>[
        Text("01"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("22 องศา"),

      ],
    );
  }

  Widget buildTwoButton() {
    return Column(
      children: <Widget>[
        Text("02"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("21 องศา"),

      ],
    );
  }

  Widget buildThreeButton() {
    return Column(
      children: <Widget>[
        Text("03"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("21 องศา"),

      ],
    );
  }

  Widget buildFourButton() {
    return Column(
      children: <Widget>[
        Text("04"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("21 องศา"),

      ],
    );
  }

  Widget buildFiveButton() {
    return Column(
      children: <Widget>[
        Text("05"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("21 องศา"),

      ],
    );
  }

  Widget buildSixButton() {
    return Column(
      children: <Widget>[
        Text("06"),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.lightBlueAccent,
          ),
          onPressed: () {},
        ),
        Text("22 องศา"),

      ],
    );
  }

  Widget DayNowListTile() {
    return ListTile(
      title: Text("วันนี้"),
      leading: Icon(Icons.cloudy_snowing),
      subtitle: Text("วันจันทร์ อุณหภูมิ 23 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }

  Widget DayOneListTile() {
    return ListTile(
      title: Text("วันที่ 1"),
      leading: Icon(Icons.cloud),
      subtitle: Text("วันอังคาร อุณหภูมิ 22 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }

  Widget DayTwoListTile() {
    return ListTile(
      title: Text("วันที่ 2"),
      leading: Icon(Icons.sunny),
      subtitle: Text("วันพุธ อุณหภูมิ 21 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }

  Widget DayTreeListTile() {
    return ListTile(
      title: Text("วันที่ 3"),
      leading: Icon(Icons.sunny),
      subtitle: Text("วันพฤหัสบดี อุณหภูมิ 21 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }

  Widget DayFourListTile() {
    return ListTile(
      title: Text("วันที่ 4"),
      leading: Icon(Icons.sunny),
      subtitle: Text("วันศุกร์ อุณหภูมิ 21 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }

  Widget DayFiveListTile() {
    return ListTile(
      title: Text("วันที่ 5"),
      leading: Icon(Icons.sunny),
      subtitle: Text("วันเสาร์ อุณหภูมิ 21 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }

  Widget DaySixListTile() {
    return ListTile(
      title: Text("วันที่ 6"),
      leading: Icon(Icons.cloud),
      subtitle: Text("วันอาทิตย์ อุณหภูมิ 22 องศา"),
      trailing: IconButton(
        icon: Icon(Icons.cloud),
        color: Colors.lightBlueAccent,
        onPressed: () {},
      ),
    );
  }


}
